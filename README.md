Assignment: Lab 4
Date: 10/8/2015

Part 1:  The professor instructed us to modifiy it to include all of our favorite songs so I did.
		 The basic structure is a JSON list of JSON objects.  Each item in the list represents a "song"
		 and the relevent information tied to it.  Each song is specifically represented by a set of key
		 value pairs where the key is the descriptor of the content in the value.  One other interesting 
		 thing about my implementation is that I created the generes value as a list to support easily
		 accessing multiple genres in the object.
		 
Part 2:  I slightly modified to html, removing the original places to add information in favor of a simple
		 div in which I load all of the content.  The content is dynamically generated based on the contents
		 of the lab4.json file.  Upon clicking the image on the page, javascript will iterate over the elements
		 in the list, and subsequently iterate over the key value pairs in the objects.  It will create formatted
		 html on the fly and add it to the page.  The html added is formatted in such a way as to still hold references
		 to the lab2 css, which for the purposes of this lab has been renamed to lab4.css.  I understand that the
		 my css for lab2 was incomplete but it was what I had to work with.  It is my understanding that the
		 goal of the lab is simply to display the content in the JSON file to the screen while still having
		 whatever css I had apply.  I believe I have done this, even though my css for lab 2 doesn't faithfully
		 recreate the desired column layout.  The final html that is produced by the code is identical to my lab2.
		 
Lab 4 Repo: https://deApollo@bitbucket.org/deApollo/lab4.git