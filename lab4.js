$( document ).ready(function() {
	$("#coverart").click(function(){
		$.get("lab4.json",function(data){
			$("#content").append("<div id=\"playlist\"></div>")
			$("#playlist").append("<ul class=\"list_of_tracks\"></div>");
			var l_bind = $(".list_of_tracks");
			$.each(data,function(key,value){
				var builder = "";
				if(key == 0)
					builder += "<li><ul id=\"item_header\">"
				builder += "<li><ul>"
				var isFav = false;
				$.each(value,function(class_n,elem_v){
					if(class_n == "favorite_song")
						isFav = true;
					else if(key == 0)
						builder += "<li class=\"" + class_n + "\">" + elem_v + "</li>"; 
					else {
						if(class_n == "album_cover")
							builder += "<li ><img src=\"" + elem_v + "\" class=\"" + class_n + "\"/></li>";
						else if(class_n == "artist_site_url")
							builder += "<li class=\"" + class_n + "\"><a href=\"" + elem_v + "\">Website</a></li>"; 
						else if(class_n == "track_name" && isFav)
							builder += "<li class=\"" + class_n + "\" id=\"favorite_song\">" + elem_v + "</li>";
						else
							builder += "<li class=\"" + class_n + "\">" + elem_v + "</li>";
					}					
				});
				builder += "</ul></li>"
				l_bind.append(builder);
			});
		});
	});
});